import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { remark } from "remark";
import html from "remark-html";

const postsDirectory = path.join(process.cwd(), "posts");

function fileNameToPostId(fileName) {
  return fileName.replace(/\.md$/, "");
}

export async function getSortedPostsData() {
  const fileNames = fs.readdirSync(postsDirectory);
  const allPostsData = await Promise.all(
    fileNames.map(async (fileName) => {
      return await getPostData(fileNameToPostId(fileName, false));
    })
  );
  return allPostsData.sort(({ date: a }, { date: b }) => {
    if (a < b) {
      return 1;
    } else if (a > b) {
      return -1;
    } else {
      return 0;
    }
  });
}

export function getAllPostIds() {
  const fileNames = fs.readdirSync(postsDirectory);
  return fileNames.map(fileNameToPostId);
}

export async function getPostData(id, includeHtml = true) {
  const fullPath = path.join(postsDirectory, `${id}.md`);
  const fileContents = fs.readFileSync(fullPath, "utf8");
  const matterResult = matter(fileContents);

  const getProcessedContent = async () => {
    const processedContent = await remark().use(html).process(matterResult.content);
    return processedContent.toString();
  };

  return Object.assign(
    {
      id,
      ...matterResult.data,
    },
    includeHtml ? { contentHtml: await getProcessedContent() } : {}
  );
}
