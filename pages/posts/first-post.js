import Link from "next/link";
import Head from "next/head";
import Script from "next/script";
import { useState } from "react";
import Layout from "/components/layout";

export default function FirstPost() {
  const [message, setMessage] = useState("");
  return (
    <Layout>
      <Head>
        <title>First Post</title>
      </Head>

      <Script
        src="https://connect.facebook.net/en_US/sdk.js"
        strategy="lazyOnload"
        onLoad={() => {
          setMessage("script loaded correctly, window.FB has been populated");
        }}
      />

      <h1>First Post</h1>
      <h2>
        <Link href="/">
          <a>Back to home</a>
        </Link>
      </h2>
      {message ? <p>{message}</p> : <></>}
    </Layout>
  );
}
